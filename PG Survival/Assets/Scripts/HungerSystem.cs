﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.FirstPerson;


[RequireComponent(typeof(Player))]
public class HungerSystem : MonoBehaviour
{

    private Player player;
    private RigidbodyFirstPersonController playerController;

    [SerializeField] private HealthSystem healthSystem;

    [SerializeField] [Range(0,5f)] private float starveRate = 0.0f;

    [SerializeField] private float starvationTime;

    [SerializeField] private float starvationDamage;

    [SerializeField] private float starveMultiplier = 1f;

    [SerializeField] private float starveStep = 1f;

    private void Start()
    {

        player = GetComponent<Player>();
        playerController = GetComponent<RigidbodyFirstPersonController>();
        healthSystem = GetComponent<HealthSystem>();
        if (player.stats.Hunger > 0)
        {
            StartCoroutine(Starve());
        }
    }


    private void Update()
    {

     if (!playerController.Running)
     {
          starveMultiplier = 2f;
     }
     else if (!playerController.Running)
     {
         starveMultiplier = 1;
     }
    }

    private IEnumerator BeginStarvation()
    {

        while (player.stats.Hunger <= 0)
        {
            yield return new WaitForSeconds(starvationTime);
            healthSystem.TakeDamage(starvationDamage);

        }

        StartCoroutine(Starve());

    }

    private IEnumerator Starve()
    {

        while (player.stats.Hunger > 0)
        {            
                yield return new WaitForSeconds(starveRate);
                player.stats.Hunger -= starveStep * starveMultiplier;
                UIManager.Instance.UpdateUIComponent(UIManager.UIComponent.HUNGERICON);
        }
        StartCoroutine(BeginStarvation());



    }


}
