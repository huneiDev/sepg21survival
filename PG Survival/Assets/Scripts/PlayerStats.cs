﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class PlayerStats
{

    [SerializeField] private float hp;

    [SerializeField] private float hunger;

    [SerializeField] private float hydration; 

    public float Hp
    {
        get { return hp; }
        set { hp = value; }

    }

    public float Hunger {
        get { return hunger; }
        set { hunger = value; }
    }

    public float Hydration {
        get { return hydration; }
        set { hydration = value; }
    }


}


