﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Player))]
[RequireComponent(typeof(HungerSystem))]
public class HealthSystem : MonoBehaviour
{

    [SerializeField] private float maxHP;

    private Player player;

    [SerializeField] private float regenerationRate;

    [SerializeField] private float regenerationTimes;


    private void Start()
    {

        player = GetComponent<Player>();
       

    }

    public void Die()
    {
        player.stats.Hp = 0;
        UIManager.Instance.SetGameOverScreen();


    }

    public IEnumerator RegenerateHp()
    {
        while (player.stats.Hp < maxHP && player.stats.Hp <= 0)
        {
            yield return new WaitForSeconds(regenerationRate);
            GiveHealth(regenerationTimes);
        }

    }

    public void GiveHealth(float amount)
    {
        if (player.stats.Hp + amount < maxHP)
        {
            player.stats.Hp += amount;
            

        }
        else
        {
            player.stats.Hp = maxHP;
        }
        UIManager.Instance.UpdateUIComponent(UIManager.UIComponent.HEALTH);
    }

    public void TakeDamage(float amount)
    {
        // 1. Nuimti damage, jei tiktais galima (Kad nebutu maziau 0)
        if (player.stats.Hp - amount >= 0)
        {
            player.stats.Hp -= amount;
            StartCoroutine(RegenerateHp());

        }
        else
        {
            
            Die();
        }
        UIManager.Instance.UpdateUIComponent(UIManager.UIComponent.HEALTH);


    }



}
