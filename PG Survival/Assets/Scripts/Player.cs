﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof(RigidbodyFirstPersonController))]
public class Player : MonoBehaviour {

    public PlayerStats stats;

    private Rigidbody rb;

    private RigidbodyFirstPersonController controller;

    [SerializeField] private float minDamageHeigth;

    [SerializeField] private HealthSystem healthSystem;

    [SerializeField] private float jumpDamageMultiplier;
    
    private float? jumpY;


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        controller = GetComponent<RigidbodyFirstPersonController>();
        healthSystem = GetComponent<HealthSystem>();

    }

    private void Update()
    {
        if (controller.Velocity.y > 0)
        {
            // Pasokom
            jumpY = transform.position.y;
        }

    }

    void OnCollisionEnter(Collision collision)
    {
        if (jumpY != null)
        {

            healthSystem.TakeDamage((float)jumpY * jumpDamageMultiplier);
            jumpY = null;
        }
       
    }




}
