﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    public enum UIComponent
    {
        HUNGERICON,
        HEALTH
       
    }

    [SerializeField] private Image hungerImage;
    [SerializeField] private Image healthImage;
    [SerializeField] private GameObject gameOverScreen;
    [SerializeField] private Player player;

    private void UpdateHungerIcon()
    {
        hungerImage.fillAmount = player.stats.Hunger/100f;

    }

    private void UpdateHealthIcon()
    {
        healthImage.fillAmount = player.stats.Hp/100f;

    }

    public void SetGameOverScreen()
    {
        gameOverScreen.SetActive(true);   
        hungerImage.gameObject.SetActive(false);
        healthImage.gameObject.SetActive(false);

    }

    public void UpdateUIComponent(UIComponent component)
    {


        switch (component)
        {
                case UIComponent.HUNGERICON:
                UpdateHungerIcon();
                break;
                case UIComponent.HEALTH:
                UpdateHealthIcon();
                break;
        }
        
        
           
    }
	

}
